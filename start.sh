export PORT=${PORT-8080}
export UUID=${UUID-1eb6e917774b4a84aff6b058577c60a5}

echo '
{
  "log": {
    "disabled": false
  },
    "inbounds": [
        {
            "type": "naive",
            "tag": "naive-in",
            "listen": "::",
            "listen_port": '$PORT',
            "users": [
               {
                    "password": "'$UUID'"
               }
              ],
              "tls": {
                "enabled": true
              }
        }
    ]
}
' > config.json

chmod +x app && ./app run
